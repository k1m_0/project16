import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InfoComponent } from './info/info.component';
import { TypeItemComponent } from './note/types/type-item/type-item.component';
import { TypeListComponent } from './note/types/type-list/type-list.component';
import { MainComponent } from './pages/main/main.component';
import { WeatherComponent } from './weather/weather.component';

const routes: Routes = [
  {
    path:'',
    component: MainComponent,
  
  },
  {
    path: 'types', component: TypeListComponent,
  },
  {
    path: 'types/type', component: TypeItemComponent,
  },
  {
    path: 'types/type/:id', component: TypeItemComponent,
  },
  {
    path:'info', component: InfoComponent,
    },
    {
    path:'weather', component: WeatherComponent,
    },
  {
    path: 'note',
    loadChildren: ()=>import('./note/note.module')
    .then((mod)=>mod.NoteModule)
  },]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
