import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Note, Type } from '../interfaces/note.interface';

@Injectable({
  providedIn: 'root'
})
export class NoteService {
  constructor(private http:HttpClient) { }

  getNotes():Promise<Note[]>{
    return this.http.get<Note[]>(`${environment.apiUrl}/notes`).toPromise();
  }

  getNote(id:number):Promise<Note>{
   return this.http.get<Note>(`${environment.apiUrl}/notes/${id}`).toPromise();
  }
  postNote(data:Note):Promise<Note>{
    return this.http.post<Note>(`${environment.apiUrl}/notes/`, data).toPromise();
   }
   
   putNote(id:number, data:Note):Promise<Note>{
    return this.http.put<Note>(`${environment.apiUrl}/notes/${id}`, data).toPromise();
  
  }
  deleteNote(id:any):Promise<Note>{
    return this.http.
    delete<Note>(`${environment.apiUrl}/notes/${id}`)
    .toPromise();
  
  }

  getTypes():Promise<Type[]>{
    return this.http.get<Type[]>(`${environment.apiUrl}/types`).toPromise();
  }
 

  getType(id:number):Promise<Type>{
   return this.http.get<Type>(`${environment.apiUrl}/types/${id}`).toPromise();
  }
  postType(data:Type):Promise<Type>{
    return this.http.post<Type>(`${environment.apiUrl}/types/`, data).toPromise();
   }
   
   putType(id:number, data:Type):Promise<Type>{
    return this.http.put<Type>(`${environment.apiUrl}/types/${id}`, data).toPromise();
  
  }
  deleteType(id:any):Promise<Type>{
    return this.http.
    delete<Type>(`${environment.apiUrl}/types/${id}`)
    .toPromise();
  
  }
  }
