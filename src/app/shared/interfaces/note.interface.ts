export interface Note{
    name:string;
    noteText:string;
    time:string;
    id?:number;
    updateTime?: string,
    noteType:number;
    showed?:boolean,
}

export interface Type{
    name:string;
    id?:number;

}