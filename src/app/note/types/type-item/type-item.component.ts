import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Type } from 'src/app/shared/interfaces/note.interface';
import { NoteService } from 'src/app/shared/services/note.service';


@Component({
  selector: 'app-type-item',
  templateUrl: './type-item.component.html',
  styleUrls: ['./type-item.component.css']
})
export class TypeItemComponent implements OnInit {
  id?: number | null = null;
  constructor(private typeService: NoteService,
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) { }
  types!: Type[];
  type!: Type;
  typeForm !: FormGroup;
  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.id = params.id ? +params.id : null;
      this.getData();
    })
  }
  async getData() {
    this.types = await this.typeService.getTypes();
    const typecontrols = {
      name: [null, [Validators.required, Validators.maxLength(100)]]
    }
    this.typeForm = this.fb.group(typecontrols);

    if (this.id) {
      try {
        this.type = await this.typeService.getType(this.id);
      } catch (error) {
        console.log(error);
      }
      this.typeForm.patchValue(this.type);
    } else {
      this.typeForm.reset();
    }


  }


  linkToItem(id?: number) {
    if (id) {
      this.router.navigate([this.router.url, "type", id]);
    } else {
      this.router.navigate([this.router.url, "type"]);
    }

  }


  async delete() {
    try {
      await this.typeService.deleteType(this.id);
      this.router.navigate(['note/types']);
    } catch (error) {
      console.log(error);
      return;
    }
  }


  async onAddType() {

    if (this.id) {
      const type: Type = this.typeForm.value;
      console.log(type);
      try {
        await this.typeService.putType(this.id, type);
        await this.getData();
      } catch (err) {
        console.log(err);
        return;
      }
    } else {
      const type: Type = this.typeForm.value;
      try {
        const typeResult = await this.typeService.postType(type);
        this.router.navigate([this.router.url, typeResult.id]);
        console.log(this.router.url);
      } catch (err) {
        console.log(err);
      }
    }
  }
}
