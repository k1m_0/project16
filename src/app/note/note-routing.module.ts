import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InfoComponent } from '../info/info.component';
import { WeatherComponent } from '../weather/weather.component';
import { NoteItemComponent } from './pages/note-item/note-item.component';
import { NoteListComponent } from './pages/note-list/note-list.component';
import { NoteLayoutComponent } from './shared/components/note-layout/note-layout.component';
import { TypeItemComponent } from './types/type-item/type-item.component';
import { TypeListComponent } from './types/type-list/type-list.component';

const routes: Routes = [
 

  {
    path: '',
    component: NoteLayoutComponent,
    children: [
      {
        path: '', component: NoteListComponent,
      },
      {
        path: 'item/:id', component: NoteItemComponent,
        // path:'item', component: PersonalListComponent
      },
      {
        path: 'item', component: NoteItemComponent,
      },
     
      {
        path: '/info', component: InfoComponent,
      },
      {
        path: '/weather', component: WeatherComponent,
      },
    ],

  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NoteRoutingModule { }
