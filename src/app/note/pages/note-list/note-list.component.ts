import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Note, Type } from 'src/app/shared/interfaces/note.interface';
import { NoteService } from 'src/app/shared/services/note.service';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.css']
})
export class NoteListComponent implements OnInit {
  notes!: Note[];
  types!: Type[];
  constructor(private noteService:NoteService, private router:Router){

  }
  getTypeName(id:number){
    // this.typer = this.types.find((x)=>x.id == this.note.noteType)
    let type =this.types?.find(x=>x.id == id);
    return type?.name
  }
  async getData(){
    try {
      this.notes = (await this.
        noteService.getNotes())||[]
    
      this.types = (await this.
        noteService.getTypes())||[]
    } catch (error) {
      console.log(error)
    }
  }
  ngOnInit(): void {
    this.getData();
  }
  linkToItem(id?:number){
if(id){
  this.router.navigate([this.router.url, 'item', id])
}else
this.router.navigate([this.router.url, 'item'])
  }


}