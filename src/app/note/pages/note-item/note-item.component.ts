import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Note, Type } from 'src/app/shared/interfaces/note.interface';
import { NoteService } from 'src/app/shared/services/note.service';

@Component({
  selector: 'app-note-item',
  templateUrl: './note-item.component.html',
  styleUrls: ['./note-item.component.css']
})
export class NoteItemComponent implements OnInit {
  id:number|null = null;
  note!: Note;
  types!:Type[];
    @Output() onSave = new EventEmitter<Note>();
    noteForm!: FormGroup;
    constructor(private fb: FormBuilder,
       private noteService:NoteService,
        private router:Router,
        private activatedRoute:ActivatedRoute
        ) { }
  
    ngOnInit(): void {
      this.activatedRoute.params.subscribe(params=> {
        this.id = params.id ? +params.id  :null
      })
    this.getData();
    };
    async getTypes() {
      try {
        this.types = (await this.
          noteService.getTypes()) || []

         
        
        } catch (error) {
        console.log(error)
      }
    }
    getTypeName(id:number){
      // this.typer = this.types.find((x)=>x.id == this.note.noteType)
      let type =this.types?.find(x=>x.id == id);
      return type?.name
    }

    

    
    
    async getData(){
      this.types = await this.noteService.getTypes();
      const controls = {
        name: [null, [Validators.required, Validators.maxLength(100)]],
        noteText: [null, [Validators.required, Validators.maxLength(1000)]],
        time: [null],
        // noteType: [null, [Validators.required]],
        noteType: [0,[Validators.required]],
        showed: [true],
        }
      this.noteForm = this.fb.group(controls);
      if (this.id){
        try {
          this.note = await  this.noteService.getNote(this.id)
          
        } catch (error) {
          
        }
        this.noteForm.patchValue(this.note)
      }else
      this.noteForm.reset();
    }
   async save() {
    if(this.id){
      this.noteForm.value.updateTime = (new Date).toLocaleString('ru')
      const note = this.noteForm.value;
      // this.onSave.emit(note);
      try {
        await this.noteService.putNote(this.id,note)
       this.getData();
      } catch (error) {
        
       }
      }else
    {
      const note = this.noteForm.value;
      // this.onSave.emit(note);
      try {
        console.log('asdas')
        this.noteForm.value.time = (new Date).toLocaleString('ru')
        const result = await this.noteService.postNote(note)
        this.router.navigate([this.router.url, result.id])
      // this.getData();
      } catch (error) {
        
      }
      this.noteForm.reset();
      // console.log('asdas')
    }
    }
    async delete() {
     
        const note = this.noteForm.value;
        // this.onSave.emit(note);
      
        try {
          await this.noteService.deleteNote(this.id)
          this.router.navigate(['/note'])
        } catch (error) {
          
         }
       
      }
  
  }
  